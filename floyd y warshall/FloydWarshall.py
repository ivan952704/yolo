
Vertices = 4

INF  = 99999
 
def floydWarshall(grafo):
    distancia = map(lambda i : map(lambda j : j , i), grafo)
     
    for k in range(Vertices):
        for i in range(Vertices):
            for j in range(Vertices):
                distancia[i][j] = min(distancia[i][j], distancia[i][k] + distancia[k][j])
	
    solucion(distancia)

 
def solucion(distancia):
    for i in range(Vertices):
        for j in range(Vertices):
            if(distancia[i][j] == INF):
                print "%4s" %("INF"),
            else:
                print "%4d" %(distancia[i][j]),
            if j == Vertices - 1:
                print ""
 
 
grafo = [
    [3, 8, INF, 13],
    [INF, 3, 9, INF],
    [INF, INF, 0, 1],
    [INF, INF, INF, 5]
]

floydWarshall(grafo);
