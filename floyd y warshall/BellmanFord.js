
function Grafo() {
	
	this.nodos = [];
	this.agregarNodo = agregarNodo;
	
	function agregarNodo(Nombre) {
		nodo = new Nodo(Nombre);
		this.nodos.push(nodo);
		return nodo;
	}
} 

function Nodo(Nombre) {
	
	this.nombre = Nombre;
	this.listaAdyacencia = [];
	this.peso = [];
	this.agregarEdge = agregarEdge;
	
	function agregarEdge(vecino, pesoCamino) {
		this.listaAdyacencia.push(vecino);
		this.peso.push(pesoCamino);	
	}
}

function bellmanFord(grafo, inicio, destino) {
	
	this.nodosAnteriores = [];
	this.distancia = new Array();				
	this.distancia[inicio.nombre] = 0;
	
	var nodos = grafo.nodos;
	var cantidad = nodos.length;
	
	for(var i = 0; i < cantidad; i++) {
		if(nodos[i] != inicio) {
			this.distancia[nodos[i].nombre] = Number.POSITIVE_INFINITY;
		}
	}
	
	for(var k = 0; k < cantidad;k ++) {
		for(var j = 0; j < cantidad; j++) {
			u = nodos[j];
			listaAdyacencia = u.listaAdyacencia;
			for (var i = 0; i < listaAdyacencia.length; i++) {
				v = listaAdyacencia[i];
				if(this.distancia[u.nombre] != Number.POSITIVE_INFINITY) {	
					alt = this.distancia[u.nombre] + u.peso[i];
					if(alt < this.distancia[v.nombre]) {

						this.nodosAnteriores[v.nombre] = u.nombre;
						this.distancia[v.nombre] = alt;
					}
				}
			}
		}
	}

	for(var j = 0; j < cantidad; j++) {
		u = nodos[j];
		listaAdyacencia = u.listaAdyacencia;
		for (var i = 0; i < listaAdyacencia.length; i++) {
			v = listaAdyacencia[i];
			if(this.distancia[u.nombre] != Number.POSITIVE_INFINITY){	
				alt = this.distancia[u.nombre] + u.peso[i];
				if(alt < this.distancia[v.nombre]) {
					return null;
				}
			}
		}
	}
	
	return this.distancia[destino.nombre];	
}

function test() {
	
	var grafo = new Grafo();
	
	nodo1 = grafo.agregarNodo("1");
	nodo2 = grafo.agregarNodo("2");
	nodo3 = grafo.agregarNodo("3");
	nodo4 = grafo.agregarNodo("4");
	nodo5 = grafo.agregarNodo("5");
	
	nodo1.agregarEdge(nodo2, 5);
	nodo1.agregarEdge(nodo4, 2);
	nodo2.agregarEdge(nodo3, 6);
	nodo4.agregarEdge(nodo3, 10);
	nodo3.agregarEdge(nodo5, 5);
	nodo5.agregarEdge(nodo2, 6);
	nodo5.agregarEdge(nodo1, 12);
	
	return bellmanFord(grafo, nodo1, nodo5)
}
