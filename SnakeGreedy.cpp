// SnakeGreedy.cpp : main project file.

#include "stdafx.h"
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <conio.h>

#define VACIO ' '
#define COLITA '.'
#define CUERPO 'O'
#define CABEZA '\1'
#define MANZANA '\3'
#define BLOQUE '#'

#define IZQUIERDA 'A'
#define DERECHA 'D'
#define ABAJO 'S'
#define ARRIBA 'W'

#define CONTINUA 0
#define MUERTE 1
#define VICTORIA 2

int manzanaX;
int manzanaY;

using namespace std;
using namespace System;

char** creaMatrixChar(int n, int m) {
	char** t = new char*[n];
	for (int i = 0; i < n; ++i) {
		t[i] = new char[m];
		for (int j = 0; j < m; ++j) {
			t[i][j] = VACIO;
		}
	}
	return t;
}

void creaArraysCharLib(char** nivel, int*& libX, int*& libY, int nFil, int nCol, int& nLib) { // considera todas las celdas libres
	libX = new int[nFil * nCol];
	libY = new int[nFil * nCol];
	nLib = 0;
	for (int i = 0; i < nFil; ++i) {
		for (int j = 0; j < nCol; ++j) {
			if (nivel[i][j] == VACIO) {
				libX[nLib] = j;
				libY[nLib] = i;
				++nLib;
			}
		}
	}
}

void poner(char** nivel, int nFil, int nCol,
	int x, int y, char elemento,
	int* libX, int* libY, int& nLib) {
	if (x >= 0 && x < nCol && y >= 0 && y < nFil) {
		nivel[y][x] = elemento;
		for (int i = 0; i < nLib; ++i) {
			if (libX[i] == x && libY[i] == y) {
				--nFil;
				libX[i] = libX[nFil]; // eliminación por intercambio del útimo elemento.
				libY[i] = libY[nFil];
				return;
			}
		}
	}
}

void creaManzana(char** nivel, int* libX, int* libY, int& num, int nFil, int nCol) {
	int pos = rand() % num;
	int x = libX[pos];
	int y = libY[pos];
	manzanaX = libX[pos];
	manzanaY = libY[pos];
	--num;
	libX[pos] = libX[num]; // eliminación por intercambio del útimo elemento.
	libY[pos] = libY[num];
	poner(nivel, nFil, nCol, x, y, MANZANA, libX, libY, num);
	Console::SetCursorPosition(x, y);
	cout << MANZANA;
}

void agregarLibre(int* libX, int* libY, int& num, int x, int y) {
	libX[num] = x;
	libY[num] = y;
	++num;
}

void imprimeNivel(char** nivel, int nFil, int nCol) {
	for (int i = 0; i < nFil; ++i) {
		for (int j = 0; j < nCol; ++j) {
			Console::SetCursorPosition(j, i);
			cout << nivel[i][j];
		}
	}
}

double distanciaManhattan(int posX, int posY) {
	double dx = abs(posX - manzanaX);
	double dy = abs(posY - manzanaY);
	return abs(dx + dy);
}

void algoritmoGreedy(char &dir, int &cabeza, int* serpienteY, int* serpienteX)
{
	double arriba = distanciaManhattan(serpienteX[cabeza], serpienteY[cabeza] - 1);
	double abajo = distanciaManhattan(serpienteX[cabeza], serpienteY[cabeza] + 1);
	double izquierda = distanciaManhattan(serpienteX[cabeza] - 1, serpienteY[cabeza]);
	double derecha = distanciaManhattan(serpienteX[cabeza] + 1, serpienteY[cabeza]);

	if (arriba <= abajo && arriba <= izquierda && arriba <= derecha)
		dir = ARRIBA;
	else if (abajo < arriba && abajo <= izquierda && abajo <= derecha)
		dir = ABAJO;
	else if (izquierda < derecha && izquierda <= arriba && izquierda <= derecha)
		dir = IZQUIERDA;
	else if (derecha < arriba && derecha <= izquierda && derecha <= abajo)
		dir = DERECHA;
}

int mover(char** nivel, int* libX, int* libY,
	int nFil, int nCol, int nLib, char dir,
	int* serpienteX, int* serpienteY, int& cola, int& cabeza) {
	int nvPosCabX = serpienteX[cabeza];
	int nvPosCabY = serpienteY[cabeza];
	poner(nivel, nFil, nCol, nvPosCabX, nvPosCabY, CUERPO, libX, libY, nLib);
	Console::SetCursorPosition(nvPosCabX, nvPosCabY);
	cout << CUERPO;

	algoritmoGreedy(dir, cabeza, serpienteY, serpienteX);

	switch (dir) {
	case IZQUIERDA: --nvPosCabX; break;
	case DERECHA: ++nvPosCabX; break;
	case ABAJO: ++nvPosCabY; break;
	case ARRIBA: --nvPosCabY; break;
	}
	if (nvPosCabX < 0 || nvPosCabX >= nCol
		|| nvPosCabY < 0 || nvPosCabY >= nFil
		|| (nivel[nvPosCabY][nvPosCabX] != VACIO
			&& nivel[nvPosCabY][nvPosCabX] != MANZANA)) {
		return MUERTE;
	}
	if (nivel[nvPosCabY][nvPosCabX] != MANZANA) { // avanzamos cola
		int posColX = serpienteX[cola];
		int posColY = serpienteY[cola];
		Console::SetCursorPosition(posColX, posColY);
		cout << VACIO;
		nivel[posColY][posColX] = VACIO;
		agregarLibre(libX, libY, nLib, posColX, posColY);
		cola = ++cola % (nFil * nCol);
		posColX = serpienteX[cola];
		posColY = serpienteY[cola];
		Console::SetCursorPosition(posColX, posColY);
		cout << COLITA;
	}
	else { // cola no avanza (serpiente crece una posicion)
		creaManzana(nivel, libX, libY, nLib, nFil, nCol);
	}
	// mover cabeza
	poner(nivel, nFil, nCol, nvPosCabX, nvPosCabY, CABEZA, libX, libY, nLib);
	cabeza = ++cabeza % (nFil * nCol);
	serpienteX[cabeza] = nvPosCabX;
	serpienteY[cabeza] = nvPosCabY;
	Console::SetCursorPosition(nvPosCabX, nvPosCabY);
	cout << CABEZA;
	int lon;
	if (cola < cabeza) {
		lon = cabeza - cola + 1;
	}
	else {
		lon = cabeza - nCol * nFil - cola + 1;
	}
	if (lon == nCol * nFil - 10) return VICTORIA; // victoria cuando la serpiente es casi del tamano del tablero
	Console::SetCursorPosition(0, 0);
	return CONTINUA;
}

int main(int argc, char** argv) {
	srand(time(0));
	int nFilas = argc > 2 ? atoi(argv[1]) : 22;
	int nColumnas = argc > 2 ? atoi(argv[2]) : 80;

	char** nivel = creaMatrixChar(nFilas, nColumnas);
	int* libresX;
	int* libresY;
	int* serpienteX = new int[nFilas * nColumnas]; // arreglos circulares
	int* serpienteY = new int[nFilas * nColumnas];
	int cola, cabeza;
	int nLib = 0;
	char tecla = ' ';
	char dir = DERECHA;
	int estado;

	creaArraysCharLib(nivel, libresX, libresY, nFilas, nColumnas, nLib);

	// inicialización manual de serpiente!
	cola = 0;
	cabeza = 3;
	serpienteX[0] = 3; serpienteY[0] = 3;
	serpienteX[1] = 4; serpienteY[1] = 3;
	serpienteX[2] = 5; serpienteY[2] = 3;
	serpienteX[3] = 6; serpienteY[3] = 3;
	poner(nivel, nFilas, nColumnas, 3, 3, COLITA, libresX, libresY, nLib);
	poner(nivel, nFilas, nColumnas, 4, 3, CUERPO, libresX, libresY, nLib);
	poner(nivel, nFilas, nColumnas, 5, 3, CUERPO, libresX, libresY, nLib);
	poner(nivel, nFilas, nColumnas, 6, 3, CABEZA, libresX, libresY, nLib);

	Console::CursorVisible = false;
	Console::Clear();
	imprimeNivel(nivel, nFilas, nColumnas);
	cout << "Presione W A S D para movimiento, X para salir";
	creaManzana(nivel, libresX, libresY, nLib, nFilas, nColumnas);
	do {
		_sleep(100);
		if (_kbhit()) {
			tecla = toupper(_getch());
			switch (tecla) {
			case 'A': dir = dir != DERECHA ? IZQUIERDA : dir; break;
			case 'D': dir = dir != IZQUIERDA ? DERECHA : dir; break;
			case 'S': dir = dir != ARRIBA ? ABAJO : dir; break;
			case 'W': dir = dir != ABAJO ? ARRIBA : dir; break;
			}
		}
		estado = mover(nivel, libresX, libresY, nFilas, nColumnas, nLib, dir, serpienteX, serpienteY, cola, cabeza);
		if (estado == MUERTE || estado == VICTORIA) {
			break;
		}
	} while (tecla != 'X');
	Console::Clear();
	if (estado == MUERTE) {
		cout << "Game Over :(\n";
	}
	else {
		cout << "Game Over :)\n";
	}
	// limpiando stuff
	for (int i = 0; i < nFilas; ++i) {
		delete[] nivel[i];
	}
	delete[] nivel;
	delete[] libresX;
	delete[] libresY;
	return 0;
}
