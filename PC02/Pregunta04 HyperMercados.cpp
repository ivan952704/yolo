
#include "stdafx.h"
#include <stdio.h>
#include <conio.h>

#define MAXIMO 1000

int N, MW, C[MAXIMO][MAXIMO], Pi[MAXIMO], Wi[MAXIMO];

int Maximo(int a, int b)
{
	return a > b ? a : b;
}

int Mochila()
{
	int i, w;
	for (i = 0; i <= N; i++)
		C[i][0] = 0;

	for (w = 0; w <= MW; w++)
		C[0][w] = 0;

	for (i = 1; i <= N; i++)
	{
		for (w = 0; w <= MW; w++)
		{
			if (Wi[i] > w)
				C[i][w] = C[i - 1][w];
			else
				C[i][w] = Maximo(C[i - 1][w], C[i - 1][w - Wi[i]] + Pi[i]);
		}
	}

	return C[N][MW];
}
void main()
{
	int suma, personas, casos, i, y;

	printf("Ingrese numero de casos de prueba: ");
	scanf("%d", &casos);

	while (casos--)
	{
		printf("Ingrese numero de objetos: ");
		scanf("%d", &N);

		printf("Ingrese precio y peso: ");
		for (i = 1; i <= N; i++)
			scanf("%d%d", &Pi[i], &Wi[i]);

		suma = 0;
		printf("Ingrese numero de personas: ");
		scanf("%d", &personas);

		while (personas--)
		{
			printf("Ingrese peso maximo: ");
			scanf("%d", &MW);
			y = Mochila();
			suma += y;
		}

		printf("%d\n", suma);
	}
	_getch();
}
