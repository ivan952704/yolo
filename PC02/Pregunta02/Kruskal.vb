﻿Module Kruskal

    Public Structure Arista

	    Public Origen As Integer
	    Public Destino As Integer
	    Public Peso As Integer

    End Structure

    Public Structure Grafo

	    Public CantidadVertices As Integer
	    Public CantidadAristas As Integer
	    Public Arista As Arista()

    End Structure
    
    Public Structure Subset

	    Public Padre As Integer
	    Public Rank As Integer

    End Structure

    Private Sub ImprimirSolucion(result As Arista(), e As Integer)

        Console.WriteLine("Arista   Peso")
	    For i = 0 To e - 1
            Console.WriteLine("{0} - {1}    {2}", result(i).Origen, result(i).Destino, result(i).Peso)
	    Next

    End Sub

    Public Function CrearGrafo(cantidadVertices As Integer, cantidadAristas As Integer) As Grafo

	    Dim grafo As New Grafo()
	    grafo.CantidadVertices = cantidadVertices
	    grafo.CantidadAristas = cantidadAristas
	    grafo.Arista = New Arista(grafo.CantidadAristas - 1) {}

	    Return grafo

    End Function

    Private Function Find(subsets As Subset(), i As Integer) As Integer

	    If subsets(i).Padre <> i Then
		    subsets(i).Padre = Find(subsets, subsets(i).Padre)
	    End If

	    Return subsets(i).Padre

    End Function

    Private Sub Union(subsets As Subset(), x As Integer, y As Integer)

	    Dim padreX As Integer = Find(subsets, x)
	    Dim padreY As Integer = Find(subsets, y)

	    If subsets(padreX).Rank < subsets(padreY).Rank Then
		    subsets(padreX).Padre = padreY
	    ElseIf subsets(padreX).Rank > subsets(padreY).Rank Then
		    subsets(padreY).Padre = padreX
	    Else
		    subsets(padreY).Padre = padreX
		    subsets(padreX).Rank += 1
	    End If

    End Sub
    
    Public Sub AlgoritmoKruskal(graph As Grafo)

	    Dim cantidadVertices As Integer = graph.CantidadVertices
	    Dim resultado = New Arista(cantidadVertices - 1) {}
	    Dim i = 0, e = 0

	    Array.Sort(graph.Arista, Function(a As Arista, b As Arista) a.Peso.CompareTo(b.Peso))

	    Dim subsets = New Subset(cantidadVertices - 1) {}

	    For v = 0 To cantidadVertices - 1
		    subsets(v).Padre = v
		    subsets(v).Rank = 0
	    Next

	    While e < cantidadVertices - 1
		    Dim siguienteArista As Arista = graph.Arista(i)
		    Dim x As Integer = Find(subsets, siguienteArista.Origen)
		    Dim y As Integer = Find(subsets, siguienteArista.Destino)

		    i += 1

		    If x <> y Then
			    resultado(e) = siguienteArista
			    e += 1
			    Union(subsets, x, y)
		    End If
	    End While
        
	    ImprimirSolucion(resultado, e)

    End Sub

    Sub Main()

        Dim graph As Grafo = CrearGrafo(4, 5)
        
        graph.Arista(0).Origen = 0
        graph.Arista(0).Destino = 1
        graph.Arista(0).Peso = 8
        
        graph.Arista(1).Origen = 0
        graph.Arista(1).Destino = 2
        graph.Arista(1).Peso = 4
        
        graph.Arista(2).Origen = 0
        graph.Arista(2).Destino = 3
        graph.Arista(2).Peso = 3
        
        graph.Arista(3).Origen = 1
        graph.Arista(3).Destino = 3
        graph.Arista(3).Peso = 13
        
        graph.Arista(4).Origen = 2
        graph.Arista(4).Destino = 3
        graph.Arista(4).Peso = 2

        AlgoritmoKruskal(graph)

        System.Console.ReadKey()

    End Sub

End Module
