﻿Module Prim

    Private Sub ImprimirSolucion(padre As Integer(), grafo As Integer(,), cantidadVertices As Integer)

	    Console.WriteLine("Arista   Peso")
	    For i = 1 To cantidadVertices - 1
		    Console.WriteLine("{0} - {1}    {2}", padre(i), i, grafo(i, padre(i)))
	    Next

    End Sub

    Private Function KeyMinimo(key As Integer(), [set] As Boolean(), cantidadVertices As Integer) As Integer

	    Dim minimo As Integer = Integer.MaxValue, indiceMinimo = 0

	    For v = 0 To cantidadVertices - 1
		    If [set](v) = False AndAlso key(v) < minimo Then
			    minimo = key(v)
			    indiceMinimo = v
		    End If
	    Next

	    Return indiceMinimo

    End Function

    Public Sub AlgoritmoPrim(grafo As Integer(,), cantidadVertices As Integer)

	    Dim padre = New Integer(cantidadVertices - 1) {}
	    Dim key = New Integer(cantidadVertices - 1) {}
	    Dim setMinimos = New Boolean(cantidadVertices - 1) {}

	    For i = 0 To cantidadVertices - 1
		    key(i) = Integer.MaxValue
		    setMinimos(i) = False
	    Next

	    key(0) = 0
	    padre(0) = -1

	    For count = 0 To cantidadVertices - 2
		    Dim u As Integer = KeyMinimo(key, setMinimos, cantidadVertices)
		    setMinimos(u) = True

		    For v = 0 To cantidadVertices - 1
			    If Convert.ToBoolean(grafo(u, v)) AndAlso setMinimos(v) = False AndAlso grafo(u, v) < key(v) Then
				    padre(v) = u
				    key(v) = grafo(u, v)
			    End If
		    Next
	    Next

	    ImprimirSolucion(padre, grafo, cantidadVertices)

    End Sub

    Sub Main()
        Dim grafo As Integer(,) = {
            {1, 4, 0, 0, 1}, 
            {3, 0, 3, 5, 5}, 
            {1, 8, 7, 0, 3},
            {0, 5, 0, 5, 1}, 
            {3, 4, 4, 8, 0}
        }

        AlgoritmoPrim(grafo, 5)

        System.Console.ReadKey()

    End Sub

End Module
