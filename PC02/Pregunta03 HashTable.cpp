
#include "stdafx.h"
#include <stdio.h>
#include <conio.h>
#include <string>
#include <iostream>

using namespace std;

const int TAMANO_MAXIMO = 128;

class DataEnlazada
{
private:
	int Key;
	string Value;
	DataEnlazada *Next;

public:
	DataEnlazada(int key, string value) {
		this->Key = key;
		this->Value = value;
		this->Next = NULL;
	}

	int GetKey() {
		return Key;
	}

	string GetValue() {
		return Value;
	}

	void SetValue(string value) {
		this->Value = value;
	}

	DataEnlazada *GetNext() {
		return Next;
	}

	void setNext(DataEnlazada *next) {
		this->Next = next;
	}
};

class HashTable
{
private:
	DataEnlazada **Tabla;

public:
	HashTable() {
		Tabla = new DataEnlazada*[TAMANO_MAXIMO];
		for (int i = 0; i < TAMANO_MAXIMO; i++)
			Tabla[i] = NULL;
	}

	void Insertar(int key, string value) {
		int hash = (key % TAMANO_MAXIMO);

		if (Tabla[hash] == NULL)
			Tabla[hash] = new DataEnlazada(key, value);
		else {
			DataEnlazada *data = Tabla[hash];

			while (data->GetNext() != NULL)
				data = data->GetNext();

			if (data->GetKey() == key)
				data->SetValue(value);
			else
				data->setNext(new DataEnlazada(key, value));
		}
	}

	void Eliminar(int key) {
		int hash = (key % TAMANO_MAXIMO);

		if (Tabla[hash] != NULL) {
			DataEnlazada *dataPrevia = NULL;
			DataEnlazada *data = Tabla[hash];

			while (data->GetNext() != NULL && data->GetKey() != key) {
				dataPrevia = data;
				data = data->GetNext();
			}

			if (data->GetKey() == key) {
				if (dataPrevia == NULL) {
					DataEnlazada *dataSiguiente = data->GetNext();
					delete data;
					Tabla[hash] = dataSiguiente;
				}
				else {
					DataEnlazada *dataSiguiente = data->GetNext();
					delete data;
					dataPrevia->setNext(dataSiguiente);
				}
			}
		}
	}

	string GetValue(int key) {
		int hash = (key % TAMANO_MAXIMO);

		if (Tabla[hash] == NULL)
			return "No existe";
		else {
			DataEnlazada *entry = Tabla[hash];

			while (entry != NULL && entry->GetKey() != key)
				entry = entry->GetNext();

			if (entry == NULL)
				return "No existe";

			return entry->GetValue();
		}
	}

	~HashTable() {
		for (int i = 0; i < TAMANO_MAXIMO; i++)
			if (Tabla[i] != NULL) {
				DataEnlazada *dataPrevia = NULL;
				DataEnlazada *data = Tabla[i];

				while (data != NULL) {
					dataPrevia = data;
					data = data->GetNext();
					delete dataPrevia;
				}
			}
		delete[] Tabla;
	}
};

void main()
{
	HashTable *hashTable = new HashTable();

	hashTable->Insertar(1, "Ivan");
	hashTable->Insertar(2, "Contreras");

	cout << hashTable->GetValue(1) << endl;
	cout << hashTable->GetValue(2) << endl;

	hashTable->Eliminar(1);
	hashTable->Eliminar(2);

	cout << hashTable->GetValue(1) << endl;
	cout << hashTable->GetValue(2) << endl;

	delete hashTable;
	_getch();
}
