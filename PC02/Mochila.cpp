
#include "stdafx.h"
#include <stdio.h>
#include <conio.h>

int** matriz = NULL;

void inicializarMatriz(int N, int M)
{
	int i, j;
	matriz = new int*[N + 1];

	for (i = 0; i < N + 1; i++)
		matriz[i] = new int[M + 1];

	for (i = 0; i <= N; i++)
		for (j = 0; j <= M; j++)
			matriz[i][j] = -1;
}

int maximo(int a, int b)
{
	return (a > b) ? a : b;
}

int mochila(int N, int M, int W[], int V[])
{
	if (matriz != NULL && matriz[N][M] != -1)
		return matriz[N][M];

	inicializarMatriz(N, M);

	int i, j;

	for (i = 0; i <= N; i++)
	{
		for (j = 0; j <= M; j++)
		{
			if (i == 0 || j == 0)
				matriz[i][j] = 0;
			else if (W[i - 1] <= j)
				matriz[i][j] = maximo(V[i - 1] + matriz[i - 1][j - W[i - 1]], matriz[i - 1][j]);
			else
				matriz[i][j] = matriz[i - 1][j];
		}
	}

	return matriz[N][M];
}

int main(array<System::String ^> ^args)
{
	int V[] = { 80, 120, 140 };
	int W[] = { 40, 50, 60 };
	int M = 40;
	int N = sizeof(V) / sizeof(V[0]);

	printf("El resultado es: %d", mochila(N, M, W, V));
	printf("\nEl resultado es: %d", mochila(N, M, W, V));

	_getch();
	return 0;
}
