#include "stdafx.h"
#include <stdio.h>
#include <conio.h>

int** matriz = NULL;

void inicializarMatriz(int N, int M)
{
	int i, j;
	matriz = new int*[N + 1];

	for (i = 0; i < N + 1; i++)
		matriz[i] = new int[M + 1];

	for (i = 0; i <= N; i++)
		for (j = 0; j <= M; j++)
			matriz[i][j] = -1;
}

int minimo(int a, int b)
{
	return (a < b) ? a : b;
}

int combinatoria(int n, int m)
{
	if (matriz != NULL && matriz[n][m] != -1)
		return matriz[n][m];

	inicializarMatriz(n, m);

	int i, j;

	for (i = 0; i <= n; i++)
	{
		for (j = 0; j <= minimo(i, m); j++)
		{
			if (j == 0 || j == i)
				matriz[i][j] = 1;
			else
				matriz[i][j] = matriz[i - 1][j - 1] + matriz[i - 1][j];
		}
	}

	return matriz[n][m];
}

int main(array<System::String ^> ^args)
{
	int n = 5, m = 2;
	printf("El valor de Combinatoria(%d, %d) is %d ", n, m, combinatoria(n, m));
	printf("\nEl valor de Combinatoria(%d, %d) is %d ", n, m, combinatoria(n, m));

	_getch();
	return 0;
}
