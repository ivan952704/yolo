// ConsoleApplicationCPlusPlus.cpp : main project file.

#include "stdafx.h"
#include <stdio.h>
#include <conio.h>

#define N 4

void ImprimirLaberinto(int laberinto[N][N])
{
	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
			printf("%d ", laberinto[i][j]);
		printf("\n");
	}
	printf("\n\n");
}

bool EsPosicionValida(int laberinto[N][N], int row, int col)
{
	if (laberinto[row][col] == 0)
		return false;
	return true;
}

bool ExisteSolucion(int laberinto[N][N], int solucion[N][N], int row, int col)
{
	if (row == N - 1 && col == N - 1)
	{
		solucion[row][col] = 1;
		return true;
	}
	
	if (EsPosicionValida(laberinto, row, col))
	{
		solucion[row][col] = 1;

		if (ExisteSolucion(laberinto, solucion, row, col + 1))
			return true;
		if (ExisteSolucion(laberinto, solucion, row + 1, col))
			return true;

		solucion[row][col] = 0;
	}

	return false;
}

void ResolverLaberinto(int laberinto[N][N])
{
	int solucion[N][N] =
	{
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 },
		{ 0, 0, 0, 0 }
	};

	if (ExisteSolucion(laberinto, solucion, 0, 0))
		ImprimirLaberinto(solucion);
	else
		printf("No existe solucion");
}

void main()
{
	int laberinto[N][N] =
	{
		{ 1, 0, 0, 0 },
		{ 1, 0, 0, 0 },
		{ 1, 1, 1, 0 },
		{ 0, 0, 1, 1 }
	};

	ResolverLaberinto(laberinto);

	_getch();
}
